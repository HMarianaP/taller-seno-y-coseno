function dibujarSeno() {

    let miLienzo = document.getElementById("lienzo");
    let superficie = miLienzo.getContext("2d");

    for (let i = 0; i <=360 ; i += 1) {

        let angulo = (i * Math.PI / 180)*(-1);


        let seno = Math.sin(3*angulo) *50 +75;

        superficie.beginPath();
        superficie.arc(i, seno, 1, 0, 2 * Math.PI, true);
        superficie.stroke()
    
        

    }

}
function dibujarCoseno() {

    let miLienzo = document.getElementById("lienzo");
    let superficie = miLienzo.getContext("2d");

    for (let i = 0; i <=360 ; i += 1) {

        let angulo = (i * Math.PI / 180)*(-1);


        let coseno = Math.cos(3*angulo) *50 +75;

        superficie.beginPath();
        superficie.arc(i, coseno, 1, 0, 2 * Math.PI, true);
        superficie.stroke()
    
        

    }


 }